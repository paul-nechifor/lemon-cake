extern dlopen
extern dlsym
extern exit

section .data
LC0: db "libc.so", 0
LC1: db "strlen", 0

global main
main:
  push rbp
  mov rbp, rsp
  sub rsp, 16
  mov esi, 1
  lea rdi, [LC0]
  call dlopen
  mov qword [rbp-8], rax
  mov rax, qword [rbp-8]
  lea rsi, [LC1]
  mov rdi, rax
  call dlsym
