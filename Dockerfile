#FROM ubuntu:14.04
FROM ubuntu:16.04
RUN apt-get update && apt-get install gcc nasm -y
COPY . /lc
WORKDIR /lc
RUN TERM=screen-256color notests=1 ./make
#RUN TERM=screen-256color debug=1 all=1 ./make
